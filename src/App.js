import React, { Component } from 'react';
// import fire from './fire';
import {LocationDetail} from './components/LocationDetail/LocationDetail';
import './style/css/index.css';

class App extends Component {
  render() {
    return (
      <div className="lights">
        <header className="lights-header">
          <h1 className="lights-title">Welcome to Lights</h1>
        </header>
        <LocationDetail />
      </div>
    );
  }
}

export default App;
