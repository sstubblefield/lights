import React, { Component } from 'react';

class LocationDetail extends Component {
    render() {
        return(
            <div className="locationDetail">
                <h2>Location Name</h2>
                <address>
                    <p>123 Elm Street<br/>
                    Tampa, FL 33613</p>
                </address>
                <button>View Map</button>

                <hr/>

                <div className="extraDetails">
                    <p className="runningHours">Running hours:<br/>
                    <span>6pm - 12am</span></p>

                    <p className="musicIncluded">Music:<br/>
                    <span>none</span></p>
                </div>
            </div>
        );
    }
};

export {LocationDetail};